

// https://www.youtube.com/watch?v=BFpLY7G3ymQ
// 16:01

class WebComp extends HTMLElement {
    constructor() {
        super();
        this.shadow = this.attachShadow({ mode: "open" });
    }
    
    get number() {
        return this.getAttribute('number')
    }

    set number(val){
        this.setAttribute('number', val)
    }

    static get observedAttributes() {
        return ["number"];
    }

   

    attributeChangedCallback(prop, oldVal, newVal){
        if(prop === 'number')
            this.render();
            let btn = this.shadow.querySelector("#button");
            btn.addEventListener("click", this.inc.bind(this));
        
    }

    inc(){
        this.count++;
        console.log("increment");
    };
    connectedCallback() {
        this.render();
        let btn = this.shadow.querySelector("#button");
        btn.addEventListener("click", this.inc.bind(this));
    }

// number is defined as atribute in html web-comp tag. In js (here) is property
    render() {
        this.shadow.innerHTML =
        `${this.number}
        <br>
        <div>
        <p> all elements within the black border are elements of custom tag shadow element </p>
        <button type="button" class="btn btn-primary" id="button">
        Increment+
        </button>
        </div>
        <style>
        *{
            border: 1px solid white;
            color: red
        }
        </style>    
        <br> 
        `;
    }
}

customElements.define('web-comp', WebComp);


function count(){
    let number = document.querySelector('#numb')
    number.setAttribute('number', 100);
    console.log("works!")
}

// call()

const personA = {
    name: 'Chuck',
    say: function(){
        console.log(this.name);
        console.log(`My surname is ${this.surname}`);
    },
    sayAge: function(age){
        console.log(`My name is ${this.name} and I am ${age}`);
    },
    sayAge2: function(status){
        console.log(`My name is ${this.name} and I am/have ${this.status ? this.status : status}`)
    }
}

const personB = {
    name: 'Becky',
    surname: 'Flint',
    status: 'single'
}

const personC = {
    name: 'Garry',
    surname: 'Connor'
}

const people = [personA,personB,personC]

function callFc(){
    //display a name from personA
   console.log(personA.name)
   // display a name from personB/ using a function "say" from personA or
   // using a personA function/method "say" display a name from personB
   console.log(personA.say.call(personB))
   // you can display a name from diffrent object ex. personC
   console.log(personA.say.call(personC))
}

function callFc2(){
      // you can also use loop for/each to display each of the people from people array
   people.forEach(p => personA.say.call(p));
}

// apply()
//  you can apply new value to the previously defined variable. In this case it is variable "age"
// in appply() I can add addictional parameters to your function
function applyFc(){
    people.forEach(p => {
        personA.sayAge.apply(p, ['24 years old'])
    });
// you can refer to existing object using "this.status" or when object(personA,personB, personC) has any status you can apply some value instead, using " this.status : status"
//
    people.forEach(p => {
        personA.sayAge2.apply(p, ['unknown status'])
    });
}

// you can use bind() when you want to add your function/method to a new variable. Instead of using square brackets you can use qoutation marks
// To execute your binded function you have to write it's name under variable
// Example below:
function bindFc(){
    people.forEach(p => {
       const bindEx =  personA.sayAge2.bind(p, 'unknown status');
       bindEx();
    })
}

function dom1(){
    // childNodes = nodelist displays all of the node of the DOM
    let childNodes = document.body.childNodes;
    // children = is a nodelist filtered by type. You will see only Elements
    console.log(typeof(childNodes));
    console.log(childNodes);
    console.log(children)
}

function dom2(){
    let div = document.getElementsByTagName("div");
    // display which type of node are div
    console.log(div[1].nodeType)
    // dispaly the name of the constructor
    console.log(div.constructor.name);

}


document.querySelector('a').addEventListener('click', (e)=>{
    e.preventDefault();
    console.log("not transfered!")
})

document.querySelector('#goto').addEventListener('click', ()=>{
    link = "http://google.com"
    window.location.replace(link);
})

// capturing phase
let timer = 0;
function showcapturing() {
    
    const one = document.querySelector('.one');
    const two = document.querySelector('.two');
    const three = document.querySelector('.three');
    const four = document.querySelector('.four');
    
    one.addEventListener('capture', event=>{
        console.log(`event (box) 1, faza ${phase(event.eventPhase)}`)
    },setTimeout(()=>{
        one.classList.add('active')
    },timer+=800),{capture:true})


    two.addEventListener('capture', event=>{
        console.log(`event (box) 2, faza ${phase(event.eventPhase)}`)
    },setTimeout(()=>{
        two.classList.add('active')
    },timer+=800),{capture:true})


    three.addEventListener('capture', event=>{
        console.log(`event (box) 3, faza ${phase(event.eventPhase)}`)
    },setTimeout(()=>{
        three.classList.add('active')
    },timer+=800),{capture:true})


    four.addEventListener('capture', event=>{
        console.log(`event (box) 4, faza ${phase(event.eventPhase)}`)
        console.log(event)
    },setTimeout(()=>{
        four.classList.add('active')
    },timer+=800))
    
    function phase(eventPhase){
        switch(eventPhase){
            case 1: 
            return 'capturing';
            case 2: 
            return 'at target';
            case 3:
            return 'bubbling'
        }
    }
    four.dispatchEvent(new CustomEvent('capture'));
}

// bubbling phase


function showbubbling(){
    let timer = 0
    const one = document.querySelector('.one');
    const two = document.querySelector('.two');
    const three = document.querySelector('.three');
    const four = document.querySelector('.four');
    
    one.addEventListener('showbubble', event=>{
        console.log(`event(box) 1, faza ${phase(event.eventPhase)}`)
      setTimeout(()=>{one.classList.add('active')}, timer+=800);
    
    })
    two.addEventListener('showbubble', event=>{
      setTimeout(()=>{two.classList.add('active')}, timer+=800);
    console.log(`event(box) 2, faza ${phase(event.eventPhase)}`)
    })
    three.addEventListener('showbubble', event=>{
      setTimeout(()=>{three.classList.add('active')}, timer+=800);
    console.log(`event(box) 3, faza ${phase(event.eventPhase)}`)
    })
    four.addEventListener('showbubble', event=>{
        console.log(`event(box) 4, faza ${phase(event.eventPhase)}`)
      setTimeout(()=>{four.classList.add('active')}, timer+=800);
    console.log(event);
    })
    
    function phase(eventPhase){
        switch(eventPhase){
            case 1: 
            return 'capturing';
            case 2: 
            return 'at target';
            case 3:
            return 'bubbling'
        }
    }
    four.dispatchEvent(new CustomEvent('showbubble', {bubbles: true}));
}


    


